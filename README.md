# IntelliJ-Hotkeys
Collection of the most useful hotkeys (and some other things) for the IntelliJ IDE for faster development.
If you use a Mac some hotkeys are working with `CMD` instead of `Ctrl`

| Hotkey | Description |
|---|---|
| `Ctrl + Shift + a` | Search for every possible option and get Hotkey-Combination if present |
| `Ctrl + Alt + l` | Reformat Code |
| `Ctrl + Alt + o` | Optimize imports |
| `Shift + F6` | Rename file/class/value under the cursor in every part in the project is used |
| `Ctrl + w` | Mark a part of the value under the cursor. After pressing it multiple times will mark more of the line and finally the complete line |
| `Ctrl + d` | Duplicate current line or marked text |
| `Ctrl + r` | Open search and replace "menu" |
| `Ctrl + Shift + r` | Global search and replace in entire project (file mask and other options are possible for finer control) |
| `Ctrl + f` | Search in file |
| `Ctrl + Shift + f` | Global search (file mask and other options are possible for finer control) |
| double tap `Shift` | Global class/filename search |
| double tap `Ctrl` (hold Ctrl at the second time) | Activate multiline selection with the arrow keys as long as ctrl is pressed |
| `Alt + j` | mark all further occurring values |
| `Ctrl + Left-Click on a value or class`| Navigate to implementation/declaration or show all places where the class or value is used |
| `Alt + F12` | Open Terminal |
| `Ctrl + Alt + S` | Open Settings |
| `Alt + Enter` | Open suggestions, the same as the little lightbulb will shown |
| `Alt + Insert` | Opens generator selection for constructor, getter, setter, override methods and so on |
| `Ctrl + Alt + ;` | Open Emoji board (well maybe it could be useful 😉)

### Some other useful tipps and tricks
 * If you have merge conflicts search with `Ctrl + Shift + a` for `Resolve Conflicts` and double left-click on conflicting file for easier resolving
 * On the right site of the line numbers there the current changes marked, click on it you can see the version prior to the current change and revert it if needed
 * On the right side there are one or two vertical tabs named `Gradle` and/or `Maven`. Open these to run tasks provided by these tools and configuration tools via mouse-click
 * right click on one of the line numbers and select `Annotate with Git Blame` to see who has last modified the code on a specific line
 * If you use Windows and want to use the Git-Bash inside IntelliJ, search inside the settings for terminal and change the default terminal path to `C:\Program Files\Git\bin\sh.exe` (this is the default install path of git bash)

 ### Editorconfig
 If you work in a team I recommend to generate an `.editorconfig` file ([Link](https://editorconfig.org/)) in your project root. You can export your Intellij project settings as an editorconfig. Intellij load this config if you open the a project with an `.editorconfig` file in it and everyone has the same config (e.g. indentation settings).

### Recommended Plugins
I like this plugins and recommend it:
 * [Rainbow Brackets](https://plugins.jetbrains.com/plugin/10080-rainbow-brackets) (makes it more clear where a code block starts and ends)
 * [Indent Rainbow](https://plugins.jetbrains.com/plugin/13308-indent-rainbow) (more visiblity support for code blocks and shows if the indentation is correct, especially useful for HTML)
 * [Key Promoter X](https://plugins.jetbrains.com/plugin/9792-key-promoter-x) (this is not an exhaustive list. This plugin will provide hotkeys for your most used tasks.)
 * [.ignore](https://plugins.jetbrains.com/plugin/7495--ignore) (simply generate .ignore-files inside your IDE and get .ignore-file support)
 * [Code With Me](https://plugins.jetbrains.com/plugin/14896-code-with-me) (I don't use it myself but could be worth a try)
